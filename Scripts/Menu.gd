extends Node2D



func _ready():
	var player_vars = get_node("/root/PlayerVariables")
	player_vars.lifes = 3
	player_vars.points = 0
	$MarginContainer/VBoxContainer/TextureButton.grab_focus()
	
	
func _process(delta):
	if $MarginContainer/VBoxContainer/TextureButton.is_hovered():
		$MarginContainer/VBoxContainer/TextureButton.grab_focus()
	if $MarginContainer/VBoxContainer/TextureButton2.is_hovered():
		$MarginContainer/VBoxContainer/TextureButton2.grab_focus()
	

func _on_TextureButton_pressed():
	get_tree().change_scene("res://Scenes/Fases/Fase1.tscn")


func _on_TextureButton2_pressed():
	get_tree().quit()
