extends KinematicBody2D

signal on_death
signal coin_collected

const UP = Vector2(0,-1)
var move = Vector2()

export var speedX = 100
export var jumpForce = 200
export var gravity = 300
export var jumpInterval = 0.05
export var maxJumps = 2
var jumps = maxJumps

var time = 0

var jumping = false

var startPosition = Vector2()

func resetTime():
	time = 0

func timer(delta):
	time += delta	
	if(time > jumpInterval):
		return true
	return false
	

func _ready():
	startPosition = position
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	move.y += gravity * delta
	
	if Input.is_action_pressed("ui_left"):
		move.x = -speedX
		if(not jumping):
			$AnimatedSprite.animation = "walking"
		$AnimatedSprite.flip_h = true
	elif Input.is_action_pressed("ui_right"):
		move.x = speedX
		if(not jumping):
			$AnimatedSprite.animation = "walking"
		$AnimatedSprite.flip_h = false
	else:
		move.x = 0
		if(not jumping):
			$AnimatedSprite.animation = "idle"

	if Input.is_action_pressed("ui_up") and timer(delta):
		jumping = true
		$AnimatedSprite.animation = "jumping"
		if is_on_floor() :
			resetTime()
			move.y = -jumpForce
		elif jumps > 0:
			resetTime()
			move.y = -jumpForce
			jumps -= 1
			
	if is_on_floor():
		jumping = !jumping
		timer(1)
		move.y = 0
		jumps = maxJumps
	else:
		pass
		
	move_and_slide(move, UP)

func _on_Coin_coin_collected():
	emit_signal("coin_collected", 10)


func _on_DeathArea_player_entered():
	position = startPosition
	emit_signal("on_death")
