extends Node2D

func _ready():
	$TextureButton.grab_focus()
	
	
func _process(delta):
	if $TextureButton.is_hovered():
		$TextureButton.grab_focus()
	

func _on_TextureButton_pressed():
	get_tree().change_scene("res://Scenes/Menu.tscn")
