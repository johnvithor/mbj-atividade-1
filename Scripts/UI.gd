extends CanvasLayer

var player_vars

func _ready():
	player_vars = get_node("/root/PlayerVariables")
	$Control/ReferenceRect/Lifes/Label.text = str(player_vars.lifes)
	$Control/ReferenceRect/Coins/Label.text = str(player_vars.points)

func _on_Player2_on_death():
	player_vars.lifes -= 1
	if player_vars.lifes == 0:
		get_tree().change_scene("res://Scenes/GameOver.tscn")
	else:
		$Control/ReferenceRect/Lifes/Label.text = str(player_vars.lifes)


func _on_Player2_coin_collected(value):
	player_vars.points += value
	$Control/ReferenceRect/Coins/Label.text = str(player_vars.points)
