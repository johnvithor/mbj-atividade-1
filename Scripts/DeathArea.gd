extends Area2D

signal player_entered

func _on_DeathArea_body_entered(body):
	if body.is_in_group("Players"):
		emit_signal("player_entered")